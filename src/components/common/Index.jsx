import Card from "./Card";
import Nav from "./NavFooter";
import Social from "./SocialMedia";
import Label from "./Label";
import ButtonComponent from "./ButtonComponent";
import PageLink from "./PageLink";
import ErrorMsg from "./ErrorMsg";

export { Card, Nav, Social, Label, PageLink, ButtonComponent, ErrorMsg };
