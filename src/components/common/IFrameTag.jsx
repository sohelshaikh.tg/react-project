import React from 'react'
import "bootstrap/dist/css/bootstrap.min.css";
import styled from "@emotion/styled";

const ContainerStyled = styled.div`
    padding: 0px;
`;

const IframeStyled = styled.iframe`
    width : ${(props) => props.width || '100%'};
    heigth : ${(props) => props.width};
    
`;

function IFrameTag({ width="", height="" }) {
  return (
    <ContainerStyled>
    <div className="container-fluid">
        <IframeStyled src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d60299.354638438286!2d72.86713660000001!3d19.164182100000012!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b707bfffffff%3A0x6ac9c4fc7911bf6b!2sOberoi%20Mall!5e0!3m2!1sen!2sin!4v1679653337437!5m2!1sen!2sin"
            width={width}
            height={height}
            allowfullscreen=""
            loading="lazy"
            referrerpolicy="no-referrer-when-downgrade"/>    
      </div>
      </ContainerStyled>
  )
}

export default IFrameTag