import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import MobileNumberIcon from "../svgs/MobileNumberIcon";
import "../../css/common/Card.css";
import PageLink from "./PageLink";
import { LinkedlnIcon, TwitterIcon, YoutubeIcon } from "../Index";

const Card = ({
  title = "",
  content = "",
  mobileNumber = false,
  socialMedia = false,
  className = "",
  twitterLink = false,
  linkedlnLink = false,
  youtubeLink = false,
}) => {
  return (
    <div className={className}>
      <div className="card-body ">
        {title && <h5 className="card-title">{title}</h5>}
        <p className="card-text">
          {content && <p className="card-text">{content}</p>}
          {mobileNumber && (
            <React.Fragment>
              <MobileNumberIcon />
              <span>{mobileNumber}</span>
            </React.Fragment>
          )}
        </p>
        {/* {socialMedia && ( */}
        <>
          {linkedlnLink && (
            <PageLink link={linkedlnLink}>
              <LinkedlnIcon />
            </PageLink>
          )}

          {twitterLink && (
            <PageLink link={twitterLink}>
              <TwitterIcon />
            </PageLink>
          )}

          {youtubeLink && (
            <PageLink link={youtubeLink}>
              <YoutubeIcon />
            </PageLink>
          )}
        </>
        {/* )} */}
      </div>
    </div>
  );
};

export default Card;
