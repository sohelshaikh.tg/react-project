import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import styled from "@emotion/styled";


const ButtonStyled = styled.button`
  margin-top: 10%;
  font-weight: ${(props) => props.weight};
  font-size: ${(props) => props.fontsize};
  line-height: 19px;
  padding: ${(props) => props.padding};
`;

const ButtonComponent = ({
  type = "",
  text = "",
  className = "",
  weight = "",
  fontsize = "",
  padding = "",
  onClick =""
}) => {
  return (
    <ButtonStyled
      text={text}
      className={className}
      type={type}
      fontsize={fontsize}
      weight={weight}
      padding={padding}
      onClick={onClick}
    >
      {text}
    </ButtonStyled>
  );
};

export default ButtonComponent;
