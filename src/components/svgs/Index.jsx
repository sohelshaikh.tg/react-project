import FacebookIcon from "./FacebookIcon";
import LinkedlnIcon from "./LinkedlnIcon";
import MobileNumberIcon from "./MobileNumberIcon";
import TwitterIcon from "./TwitterIcon";
import YoutubeIcon from "./YoutubeIcon";

export {
  FacebookIcon,
  LinkedlnIcon,
  MobileNumberIcon,
  TwitterIcon,
  YoutubeIcon,
};
