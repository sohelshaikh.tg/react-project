import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../css/form/TextArea.css";
import styled from "@emotion/styled";

const TextAreaStyled = styled.textarea`
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  font-size: ${(props) => props.fontsize};
  border: none;
  background: none;
  outline: none;
  margin-top: 12px;
  border-bottom: 1px solid #828282;
  margin: 50px 0 0;

  &::placeholder {
    font-weight: ${(props) => props.placeholderweight};
    font-size: ${(props) => props.placeholderfontsize};
    line-height: 19px;
    letter-spacing: 0.02em;
    color: rgba(80, 80, 80, 0.4);
  }
`;

const TextArea = ({
  className = "",
  placeholder = "write something",
  row = "",
  width = "",
  height = "",
  fontsize = "",
  placeholderweight = "",
  placeholderfontsize = "",
}) => {
  return (
    <TextAreaStyled
      width={width}
      row={row}
      className={className}
      placeholder={placeholder}
      height={height}
      fontsize={fontsize}
      placeholderweight={placeholderweight}
      placeholderfontsize={placeholderfontsize}
    />
  );
};

export default TextArea;
