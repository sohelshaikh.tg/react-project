import React from "react";
import styled from "@emotion/styled";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../css/form/FormInputs.css";

const FormInputStyled = styled.input`
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  font-size: 1rem;
  border: none;
  background: none;
  outline: none;
  margin-top: 12px;
  border-bottom: 1px solid #828282;
  margin: 50px 0 0;

  &::placeholder {
    font-weight: ${(props) => props.placeholderweight};
    font-size: ${(props) => props.placeholderfontsize};
    line-height: 19px;
    letter-spacing: 0.02em;
    color: rgba(80, 80, 80, 0.4);
  }
`;

const FormInputs = ({
  className = "",
  typeOfInput = "text",
  placeholder = "Write something",
  height = "",
  width = "",
  placeholderweight = "",
  placeholderfontsize = "",
  onChange,
}) => {
  return (
    <FormInputStyled
      type={typeOfInput}
      placeholder={placeholder}
      height={height}
      width={width}
      placeholderweight={placeholderweight}
      placeholderfontsize={placeholderfontsize}
      className={`input-field ${className ? className : "message-box"}`}
      onChange={onChange}
    />
  );
};
export default FormInputs;
